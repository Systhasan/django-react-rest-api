import React, { Component, Fragment } from 'react'
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { getBlogs } from '../../actions/blogs';


export class Blog extends Component {

    static propTypes = {
        blogs: PropTypes.array.isRequired
    }

    componentDidMount() {
        this.props.getBlogs();
    }

    render() {
        return (
            <Fragment>
                <h1>Articles are: </h1>
                <table className="table table-striped">
                    <thead>
                        <tr>
                            <th> ID </th>
                            <th> Title </th>
                            <th> Details </th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.props.blogs.map(blog => (
                            <tr key={blog.id}>
                                <td> {blog.id} </td>
                                <td> {blog.title} </td>
                                <td> {blog.details} </td>
                            </tr>
                        ))}
                    </tbody>
                </table>
            </Fragment>
        )
    }
}

const mapStateToProps = state => ({
    blogs: state.blogs.blogs
});

export default connect(mapStateToProps, { getBlogs })(Blog);