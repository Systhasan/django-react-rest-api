import React, { Fragment } from 'react'
import Blog from './Blog'
import BlogForm from './BlogForm'

export default function Dashboard() {
    return (
        <Fragment>
            <Blog />
            <BlogForm />
        </Fragment>
    )
}
