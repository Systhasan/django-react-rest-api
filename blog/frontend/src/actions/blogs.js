import axios from 'axios';

import { GET_BLOGS } from './types';

// get blogs
export const getBlogs = () => dispatch => {
    axios.get('/api/article/')
        .then(res => {
            dispatch({
                type: GET_BLOGS,
                payload: res.data
            })
        }).catch(err => console.log(err));
}