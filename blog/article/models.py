from django.db import models


# Create your models here.
class Article(models.Model):
    title = models.CharField(max_length=255)
    details = models.TextField()
    created_date = models.DateTimeField(auto_now_add=True)
    modified_date = models.DateTimeField(auto_now_add=True, blank=True)
    
    